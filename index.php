<!DOCTYPE html>
<html>

<head>
    <title>Entrainement BDD Php</title>
</head>

<body>

    <?php

    //Sqli ou PDO, base: J'importe ma bDD sur phpmyadmin, celle déjà comprise ds l'exo.


    //Avant pour que tout fonctionne, on paramètre php my admin https://doc.ubuntu-fr.org/phpmyadmin   // la utilisateur root défini dans les compte utilisateurs phpmy auparavant et mdp habituel
    //https://docs.phpmyadmin.net/fr/latest/setup.html#linux-distributions

    // Pour faire apparaitre l'interface phpmyadmin : Navigateur:  http://localhost/phpmyadmin/ utilisateur root et mdp habituel Sql
    //on revoit https://github.com/simplonco/php-training/blob/master/courses/Tutoriel%20MySQL%20-%20Introduction.pdf 
    // Cela permet de créer des Bdd et de les remplir // Pour se connecter au serveur local et administrer mes BDD 
    //Lancer ma page cd mon dossier et ds terminal php -S <localhost:8000>
    //ordinateur/var/www/html (copie du .php ici pr testerlocalhost/index.php ds le nav)

    //Sqli1 **etape1** CONNEXION AU SERVEUR de Bdd en php classique sur une page index.php 
    //DEFINIR SUR QUELLE BASE JE TRAVAILLE 
    /*DEFINE(SERVEUR, "localhost");
DEFINE(LOGIN, "root");
DEFINE(MDP, "Simplon007!");
DEFINE(BASE, "colyseum");

//Sqli **etape2** Gestion des erreurs de connexions sans l'orienté objet

$connect = mysqli_connect(SERVEUR, LOGIN, MDP, BASE)
    or die("Vous ne pouvez pas vous connecter au serveur, il y a un problème de connexion,
     vérifiez votre serveur, votre nom d'utilisateur de bdd déclaré ci-dessus, votre mot de passe .. ");

//Sqli **etape3** REQUETE: Afficher ts les clients de la BDD 
//AFFICHER QQCHOSE C'EST SE SERVIR DE LA NOTION [de CRUD] de READ=SELECT en SQL et (méthode GET en HTTP )
$result = mysqli_query($connect, 'SELECT*FROM clients'); //Sélectionner toutes les colonnes de la table clients
while ($data = mysqli_fetch_assoc($result)) {
    echo "Prénom du client:" . $data['firstName'] . "<br />";
    echo "Nom du client:" . $data['lastName'] . "<br /><br />";
}

//Sqli **etape4** Entrée des données dans la table user
//Entrer DES DONNEES c'EST SE SERVIR DE LA NOTION [de CRUD] de CREATE=INSERT en Sql (méthode POST en HTTP)
$result = mysqli_query($connect, "INSERT INTO user(login,mdp) VALUES ('Weekend','acoder')");
//Sélectionner toutes les colonnes de la table login
echo "Nouveau login Enregistré " . "<br /><br />";
*/

    //Afficher les données de la table user en SQli
    /*$result = mysqli_query($connect, 'SELECT*FROM user'); //Sélectionner toutes les colonnes de la table login
while ($data = mysqli_fetch_assoc($result)) {
    echo "login:" . $data['login'] . "<br />";
    echo "mdp:" . $data['mdp'] . "<br /><br />";
}*/

    // Supprimer un élément de ma table
    /*
$result = mysqli_query($connect, "DELETE FROM user WHERE 4 LIMIT 1"); //Supprimer dernier élément
while ($data = mysqli_fetch_assoc($result)) {
echo "login:" . $data['login'] . "<br />";
echo "mdp:" . $data['mdp'] . "<br /><br />";
}
*/

    //Tuto 2nd https://github.com/simplonco/php-pdo-training/blob/master/courses/Tutoriel%20MySQL%20II%20-%20PDO.pdf

    //CONNEXION AU SERVEUR de BDD EN PHP ORIENTE OBJET PDO //PDO GESTION DES ERREURS DE CONNEXION
    try {
        $dbh = new PDO('mysql:host=localhost;dbname=colyseum', 'root', 'Simplon007!');
    } catch (PDOException $e) {
        print "Erreur! Petit malin tu n'as pas réussi à te connecter à ta BDD!!" . $e->getMessage() . "<br/>";
        die();
    }

    //SECONDE ETAPE: REQUêTES 
    // ##Exercice 1 :   Afficher tous les clients. Récupérer des éléments de la classe PDO avec la méthode query
    echo "<h1>Prénom et Nom de tous mes clients</h1></br>";

    //Methode 1
    $sql = 'SELECT * FROM clients';
    $req = $dbh->query($sql);
    while ($row = $req->fetch()) {
        echo $row['firstName'] . ' ' . $row['lastName'] . '<br/>';
    };
    $req->closeCursor();

    /*Methode 2:
foreach ($dbh->query('SELECT*from clients') as $row) {
    print_r($row['firstName'] . ' ' . $row['lastName'] . '<br/>');
}*/

    //##Exercice 2 : Afficher tous les types de spectacles possibles.
    echo "<H1>Les types de spectacles disponibles à l'affiche </H1>";
    $sql = 'SELECT * FROM genres';
    $req = $dbh->query($sql);
    while ($row = $req->fetch()) {
        echo utf8_encode($row['genre']) . ' ' . '<br/>'; //à chaque echo où l'on veut du francais, préciser utf8 comme ça pas de soucis
    };
    $req->closeCursor();
    //##Exercice 3 : Afficher les 20 premiers clients.

    echo "<H1>Afficher que les 20 premiers clients</H1>";

    $sql = 'SELECT firstName,lastName
        FROM clients
        LIMIT 0,19';
    $req = $dbh->query($sql);
    while ($row = $req->fetch()) {
        echo utf8_encode($row['firstName']) . ' ' . $row['lastName'] . '<br/>';
        //à chaque echo où l'on veut du francais, préciser utf8 comme ça pas de soucis
    };
    $req->closeCursor();


    //##Exercice 4 : N'afficher que les clients possédant une carte de fidélité.

    echo "<H1>Afficher que les clients qui ont une carte de fidélité</H1>";

    $sql = 'SELECT firstName,lastName,card
        FROM clients
        WHERE card=1';
    $req = $dbh->query($sql);
    while ($row = $req->fetch()) {
        echo utf8_encode($row['firstName']) . ' ' . $row['lastName'] . '<br/>';
        //à chaque echo où l'on veut du francais, préciser utf8 comme ça pas de soucis
    };
    $req->closeCursor();

    ##Exercice 5 Afficher uniquement le nom et le prénom de tous les clients dont le nom 
    //commence par la lettre "M". Les afficher comme ceci :
    //Nom : Nom du client Prénom : Prénom du client
    //Trier les noms par ordre alphabétique.

    echo "<H1>Afficher uniquement les clients dont le nom commence par M et triés par ordre alphabétique</H1>";

    $sql = "SELECT firstName,lastName FROM clients WHERE lastName LIKE 'M%' ORDER BY lastName";
    $req = $dbh->query($sql);
    while ($row = $req->fetch()) {
        echo utf8_encode('Nom:' . $row['lastName']) . ' ' . 'Prénom:' . $row['firstName'] . '<br/>';
    };
    $req->closeCursor();

    ##Exercice 6 Afficher le titre de tous les spectacles ainsi que l'artiste, la date et
    // l'heure. Trier les titres par ordre alphabétique. Afficher les résultat comme ceci : 
    // Spectacle par artiste, le date à heure.

    echo "<H1>Afficher le titre de tous les spectacles et l'heure</H1>";

    $sql = 'SELECT title,startTime,date
            FROM shows';
    $req = $dbh->query($sql);
    while ($row = $req->fetch()) {
        echo utf8_encode($row['date']) . ' ' . $row['title'] . ' ' .  $row['startTime'] . '<br/>';
        //à chaque echo où l'on veut du francais, préciser utf8 comme ça pas de soucis
    };
    $req->closeCursor();

    /*##Exercice 7 Afficher tous les clients comme ceci : Nom : Nom de famille du client 
    Prénom : Prénom du client Date de naissance : Date de naissance du client 
    Carte de fidélité : Oui (Si le client en possède une) ou Non (s'il n'en possède pas) 
    Numéro de carte : Numéro de la carte fidélité du client s'il en possède une.*/
    /*Si je mets le nom de chaque colonne ds la boucle while, cela répète la ligne. Pour 
    la mettre une seule fois je l'écris avant ma boucle while*/

    echo "<H1>Afficher tous les clients comme ceci : Nom : Nom de famille du client </H1>";

    $sql = 'SELECT firstName,lastName,card,cardNumber FROM clients';
    $req = $dbh->query($sql);
    echo "<table border=\"1\">";
    echo "<tr>";
    echo "<td >" . 'Prénom: ' . "</td>" . "<td>" . ' Nom: ' . '</td> ' . '<td>' . 'Carte de fidélité: ' . '</td>' . '<td>' . 'Numéro de carte' . '</td>';
    echo "</tr>";
    while ($row = $req->fetch()) {

        echo "<tr>";
        echo "<td >" . 'Prénom: ' . utf8_encode($row['firstName']) . "</td>" . "<td>" . ' Nom: ' . $row['lastName'] . ' ' . '</td>';
        if ($row['card'] == 0) {
            echo '<td>' . 'Carte de fidélité: ' . " non " . '</br>' . '</td>';
        } else {
            echo '<td>' . 'Carte de fidélité: ' . " oui " . ' ' . '</td>' . '<td>' . ' Numéro de carte:' . ($row['cardNumber']) . '</br>' . '</td>';
        }
        echo "</tr>";
    };

    echo "</table>";
    $req->closeCursor();


    /*$sql = 'SELECT lastName, firstName, cardNumber FROM clients, cards';
    $req = $dbh->query($sql);
    while ($row = $req->fetch()) {
        echo utf8_encode($row['lastName']) . ' ' . $row['firstName'] . ' ' .  $row['startTime'] . '<br/>';
        };
    $req->closeCursor();*/

    //Fermeture de l'appel à la BDD
    $dbh = null; // Permet de fermer la connexion au serveur lorsque le programme est terminé.

    ?>
</body>

</html>